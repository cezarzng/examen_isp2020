package testisp;


public class extest {

	public static void main(String[] args) {
		Circle c1 = new Circle();
		Circle c2 = new Circle(2);
		Circle c3 = new Circle(3,"blue");
		System.out.println("Cercul c1 are raza: " + c1.getRadius()+" si aria: "+c1.getArea());
		System.out.println("Cercul c2 are raza: " + c2.getRadius()+" si aria: "+c2.getArea());
		System.out.println("Cercul c3 are raza: " + c3.getRadius()+" si aria: "+c3.getArea());
	}

}

class Circle {
	private double radius;
	private String color;
	Circle()
	{
		radius = 1.0;
		color = "red";
	}
	Circle(double radius)
	{
		this.radius = radius;
		this.color = "red";
	}
	Circle(double radius, String color)
	{
	 this.radius = radius;
	 this.color = color;
	}
	public double getRadius() 
	{
		return radius;
	}
	public double getArea() 
	{
		return getRadius()*getRadius()*3.14;
	}
}